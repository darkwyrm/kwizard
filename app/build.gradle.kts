plugins {
    alias(libs.plugins.jvm)

    id("org.openjfx.javafxplugin") version "0.0.14"
    `jvm-test-suite`

    application
}

repositories {
    mavenCentral()
    maven {
        setUrl("https://plugins.gradle.org/m2/")
    }
}

dependencies {
    // JavaFX
    implementation("org.openjfx:javafx-plugin:0.0.14")
    implementation("org.openjfx:javafx-base:22")
    implementation("org.openjfx:javafx-controls:22")

    // JUnit 5 with Kotlin
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit5")
    testImplementation(libs.junit.jupiter.engine)
    testRuntimeOnly("org.junit.platform:junit-platform-launcher")
}

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(17))
    }
}

javafx {
    modules("javafx.controls", "javafx.web")
}

application {
    mainClass.set("kwizard.AppKt")
}

tasks.named<Test>("test") {
    useJUnitPlatform()
}
